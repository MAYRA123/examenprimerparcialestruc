package examendos;
     public class pila {
     char arr[];
    int top;
    
    pila(){
        this.arr = new char[4];
        this.top = -1;
    }
    public boolean isEmpty(){
        return (top == -1);
    }
    public void push(char car){
        top++;
        if(top < arr.length){
            arr[top] = car;
        }else{
            char m[] = new char[arr.length+5];
            for(int i = 0; i<arr.length; i++){
                m[i] = arr[i];
            }
            arr = m; 
            arr[top] = car;
        }
    }
    public char peek(){
        return arr[top]; }
    public char pop(){
        if(!isEmpty()){
            int temptop = top;
            top--;
            char returntop = arr[temptop];
            return returntop;
        }else{
            return '-';
        }
    }
    
}
