/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenesuno;

/**
 *
 * @author pc
 */
public class pila {
    private Object pila[];
    private Object dato;
    private int tope = 0;
    private int max = 0;
    public pila(int max) 
    {
        this.max = max;
        pila = new Object[max];
        dato = "";
    }
    public boolean llena() 
    {
        return tope == max;
    }
 
    public boolean vacia() 
    {
        return tope == 0;
    }
 
    public void Apilar(Object dato) 
    {
        if(!llena())
        {
            pila[tope] = dato;
            tope++;
        }
    }
 
    public Object Desapilar() 
    {
       if(!vacia())
       {
           tope--;
            dato = pila[tope];
        }
        return dato;
    }
 
    public Object comparar() 
    {
        Object top="";
        if(!vacia())
            top = pila[tope-1];
        return top;
    } 
}
